import { GitVersion } from './git-version'
import { MockCli } from './process/testing'

describe('GitVersion', () => {
  let cli: MockCli

  beforeEach(() => {
    cli = MockCli.noTags()
  })

  describe('Release tag', () => {
    test('No release tag', () => {
      expect(GitVersion.get(cli).toString()).toEqual('0.0.0')
    })

    test('Release tag 4.0.0', () => {
      cli.tag('4.0.0')

      expect(GitVersion.get(cli).toString()).toEqual('4.0.0')
    })

    test('Release tag v4.0.0', () => {
      cli.tag('v4.0.1')

      expect(GitVersion.get(cli).toString()).toEqual('4.0.1')
    })
  })

  describe('Dirty', () => {
    beforeEach(() => {
      cli.tag('1.0.1')
    })

    test('Not dirty', () => {
      expect(GitVersion.get(cli).toString()).toEqual('1.0.1')
    })

    test('Dirty', () => {
      cli.modify()

      expect(GitVersion.get(cli).toString()).toEqual('1.0.1-dirty')
    })
  })
})
