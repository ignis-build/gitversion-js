import { Dirty } from './versions/dirty'
import { Cli } from './process/cli'
import { Version } from './versions/version'

export class GitVersion {
  constructor (private readonly version: Version, private readonly dirty: Dirty) {
  }

  static get (cli: Cli = new Cli()): GitVersion {
    const dirty = Dirty.get(cli)
    const version = Version.get(cli)

    return new GitVersion(version, dirty)
  }

  toString (): string {
    return `${this.version.toString()}${this.dirty.toString()}`
  }
}
