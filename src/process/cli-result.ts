export class CliResult {
  constructor (readonly code: number, readonly stdout: string, readonly stderr: string) {
  }

  static success (stdout: string): CliResult {
    return new CliResult(0, stdout, '')
  }

  static error (code: number, stderr: string): CliResult {
    return new CliResult(code, '', stderr)
  }

  static from (e: { readonly status: number, readonly stdout: string, readonly stderr: string }): CliResult {
    return new CliResult(e.status, e.stdout, e.stderr)
  }

  succeeded (): boolean {
    return this.code === 0
  }
}
