import { CliResult } from './cli-result'
import * as child_process from 'child_process'

export class Cli {
  exec (command: string): CliResult {
    try {
      const result = child_process.execSync(command, { encoding: 'utf-8' })
      return CliResult.success(result)
    } catch (e) {
      return CliResult.from(e)
    }
  }
}
