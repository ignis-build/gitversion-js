import { type Cli } from '../cli'
import { CliResult } from '../cli-result'
import { Mock } from './mock'
import { MockCollection } from './mock-collection'

export class MockCli implements Cli {
  private readonly mocks = new MockCollection()

  exec (command: string): CliResult {
    const found = this.mocks.find(command)
    if (found == null) throw new Error('No mock found')
    return found.returns
  }

  mock (command: string, returns: CliResult): this {
    this.mocks.add(new Mock(command, returns))
    return this
  }

  tag (name: string): this {
    this.mock('git describe --tags', CliResult.success(`${name}
`))
    return this
  }

  modify (): this {
    this.mock('git clean -n', CliResult.success('Would remove file\n'))
    return this
  }

  static noTags (): MockCli {
    return new MockCli()
      .mock('git describe --tags', CliResult.error(128, 'fatal: No names found, cannot describe anything.\n'))
      .mock('git clean -n', CliResult.success(''))
  }
}
