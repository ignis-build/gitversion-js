import { type Mock } from './mock'

export class MockCollection {
  private readonly mocks: Mock[] = []

  find (command: string): Mock | undefined {
    return this.mocks.find(m => m.match(command))
  }

  add (mock: Mock): void {
    this.mocks.unshift(mock)
  }
}
