import { type CliResult } from '../cli-result'

export class Mock {
  constructor (private readonly command: string, readonly returns: CliResult) {
  }

  match (command: string): boolean {
    return this.command === command
  }
}
