import { type Cli } from '../process/cli'

export class Dirty {
  private constructor (private readonly s: string) {
  }

  static get (cli: Cli): Dirty {
    const result = cli.exec('git clean -n')
    // const result = git.clean(['-n'])
    if (result.stdout === '') return new Dirty('')
    return new Dirty('-dirty')
  }

  toString (): string {
    return this.s
  }
}
