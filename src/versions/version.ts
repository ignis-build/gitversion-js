import { type Cli } from '../process/cli'

export class Version {
  constructor (private readonly s: string) {
  }

  static get (cli: Cli): Version {
    const result = cli.exec('git describe --tags')
    if (result.succeeded()) return new Version(result.stdout.trim().replace(/^v/, ''))
    return new Version('0.0.0')
  }

  toString (): string {
    return this.s
  }
}
